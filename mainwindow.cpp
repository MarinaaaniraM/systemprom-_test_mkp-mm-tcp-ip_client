#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QTextCodec* codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForTr(codec);

    // ========================================================================
    // Определение переменных для работы с xml файлом с помощью QDom.
    // Файл для записи текущих ip адресов и портов.
    QFile file("saves.xml");
    file.open(QIODevice::ReadOnly);

    QDomDocument saves;
    saves.setContent(&file);
    QDomElement netInfo = saves.documentElement();
    QDomNode x[NUMBER_OF_CONNECTIONS];
    x[0] = netInfo.firstChild();    // Особое внимание, продолжение ниже


    // ========================================================================
    // Определение переменных для добавление виджетов в виде сетки.
    centralWidget = new QWidget(this);
    grid = new QGridLayout();

    centralWidget->setLayout(grid);
    centralWidget->setFocus();


    // ========================================================================
    // Кнопка Сброс. Одна на всю программу. Сбрасывает все поля.
    spillingButton = new QPushButton(tr("Сброс"), this);
    grid->addWidget(spillingButton, 3, 0);
    spillingButton->setMinimumSize(15, 15);


    // ========================================================================
    // Кнопки Подключить все и Отсоединить все. Одни на всю программу.
    connectAllButton = new QPushButton(tr("Подключить все"), this);
    disconnectAllButton = new QPushButton(tr("Отсоединить все"), this);
    grid->addWidget(connectAllButton, 3, 2, 1, 2);
    grid->addWidget(disconnectAllButton, 3, 4, 1, 2);
    connectAllButton->setMinimumSize(30, 15);
    disconnectAllButton->setMinimumSize(15, 15);


    // ========================================================================
    // Цикл, равный количеству портов в устройсте.
    // В нем определяются все остальные переменные.
    for (int i=0; i<NUMBER_OF_CONNECTIONS; i++)

    {
        // ====================================================================
        // Объекты потоков и графического класса для сокетов.
        thread[i] = new Socket_Thread(this);
        form[i] = new Graphics_form(this);


        connect(spillingButton, SIGNAL(clicked()), thread[i], SLOT(spilling()));
        connect(connectAllButton, SIGNAL(clicked()), thread[i], SLOT(connectExternal()));
        connect(disconnectAllButton, SIGNAL(clicked()), thread[i], SLOT(disconnectExternal()));


        // ====================================================================
        // Над каждым столбцом название физического порта (порта устройства).
        form[i]->setPortName("X" + QString::number(i + 1));


        // ====================================================================
        // Если xml файл не существовал, то выводим адреса по умолчанию.
        if (!file.exists())
        {
            form[i]->setAdress("192.169.1.1");
            form[i]->setPort(2001 + i);
        }
        // ====================================================================
        // Если существовал, то загружаем ip адреса и порты из xml файла.
        else
        {
            if (i != 0)
            {
                // Такие дела с QDom, внимательно
                x[i] = x[i - 1].nextSibling();
            }

            form[i]->setAdress(x[i].toElement().attribute("adr"));
            form[i]->setPort(x[i].toElement().attribute("port").toUInt());
        }    


        // ====================================================================
        // Настройка компоновки и размеров окна программы.
        // Чтобы столбцы выводились в два ряда.
        int temp = NUMBER_OF_CONNECTIONS*0.5;

        // Выводим заголовки (описания полей с данными) в каждый ряд.
        if (i == 0 || i == 1)
        {
            title[i] = new Graphics_form(this);
            grid->addLayout(title[i]->getTitleLayout(), i, 0);
        }

        // Размеры окна программы. Все компактно!
        this->setFixedSize(115*temp, 71*temp);
        grid->addLayout(form[i]->getDataLayout(), i/temp, i%temp + 1);


        // ====================================================================
        // Передает объекты формы в класс потоков,
        // чтобы там в них писались данные с сокета.
        thread[i]->setGraphicsForm(form[i]);
        thread[i]->start();
    }

    setCentralWidget(centralWidget);

    file.close();
}


MainWindow::~MainWindow()
{
    delete grid;
    delete ui;
}

//=============================================================================
// Нажимаем на крестик программы и вызывается этот слот.
void MainWindow::closeEvent(QCloseEvent *event)
{
    //=========================================================================
    // Уже знакомый по размеру цикл.
    for (int i=0; i<NUMBER_OF_CONNECTIONS; i++)
    {
        //=====================================================================
        // Выключаем потоки и ждем, пока они завершатся.
        thread[i]->quit();
        thread[i]->wait(100);


        //=====================================================================
        // Уже упоминавшийся файл, но для записи текущий адресов.
        // Все зеркально и опять с помощью QDom.
        QFile file("saves.xml");
        file.open(QIODevice::WriteOnly);

        QDomDocument saves;
        QDomElement netInfo = saves.createElement("Net_info");
        QDomElement x[NUMBER_OF_CONNECTIONS];
        saves.appendChild(netInfo);


        //=====================================================================
        // Снова цикл. Подобно чтению из xml происходит запись в него.
        for (int i=0; i<NUMBER_OF_CONNECTIONS; i++)
        {
            x[i] = saves.createElement("X" + QString::number(i + 1));
            netInfo.appendChild(x[i]);
            x[i].setAttribute("adr", form[i]->getAddress()->text());
            x[i].setAttribute("port", form[i]->getPort()->text());
        }

        QTextStream(&file) << saves.toString();

        file.close();
    }
    QMainWindow::closeEvent(event);
}







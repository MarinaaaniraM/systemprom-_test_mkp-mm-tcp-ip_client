#include "tcp_ip_connection.h"

TCP_IP_Connection::TCP_IP_Connection(QObject *parent) :
    QObject(parent)
{
    sock = new QTcpSocket(this);
    timerUploads = new QTimer(this);
    timerNoUploads = new QTimer(this);


    // ========================================================================
    // Начальные данные.
    tempMessage.clear();
    uploadBytes = 0;
    downloadBytes = 0;
    allUpBytes = 0;
    allDownBytes = 0;
    errorsOfData = 0;
    speed = 0;
    nextSymbolToWrite = 0;
    symbol = 0;
    tempUploads = 0;
    tempSpeed = 0;
    paused = false;
    oneTry = true;
    sendPacket = ORIGINAL_BUFFER_PACK;

    // ========================================================================
    // Сигналы и слоты (Qt style).
    connect(sock, SIGNAL(connected()), this, SLOT(writeDataToServer()));
    connect(sock, SIGNAL(readyRead()), this, SLOT(readDataToServer()));
    connect(sock, SIGNAL(disconnected()), this, SLOT(disconnectFromServer()));
    connect(timerUploads, SIGNAL(timeout()), this, SLOT(analyzingData()));
    connect(timerNoUploads, SIGNAL(timeout()), this, SLOT(analyzingData()));
    connect(this, SIGNAL(analyzingIsComplete()), this, SLOT(writeDataToServer()));
}


// ============================================================================
// Подключает к хосту и если неудача, то возвращает false, иначе true.
bool TCP_IP_Connection::connectToHost(QString adress, quint16 port)
{
    sock->connectToHost(adress, port);
    if (sock->waitForConnected(1000))
    {
        return true;
    }
    else
    {
        return false;
    }
}


// ============================================================================
// Мгновенное отсоединение от хоста.
void TCP_IP_Connection::disconnectFromHost()
{
    sock->abort();
}


// ============================================================================
// Посылает заданную последовательность на сервер и записывается
// количество отправленных байт.
void TCP_IP_Connection::writeDataToServer()
{
//    qDebug() << "writeDataToServer";
    if (!paused)
    {
        QByteArray bufferToWrite;
        tempUploads = 0;
        // Проверка состояния соединения (3 == подключено, все ОК).
        if (sock->state() == 3)
        {
            // Засекается 1 секунда - такты будут раз в секунду.
            timerUploads->start(1000);

            for (quint16 k=0; k<sendPacket/ORIGINAL_BUFFER_PACK; k++)
            {
                // Данные посылаем кусками. Соответствующий буфер.
                bufferToWrite.clear();
                for (quint16 i=0; i<ORIGINAL_BUFFER_PACK; i++)
                {
                    if (nextSymbolToWrite == 256)
                    {
                        nextSymbolToWrite = 0;
                    }
                    quint16 testSymbol = nextSymbolToWrite;
                    nextSymbolToWrite ++;
                    bufferToWrite.append(testSymbol).toUInt();
                }
                tempUploads += sock->write(bufferToWrite);
            }
        }
    }
}


// ============================================================================
// Считывается сообщение с сервера в QByteArray.
void TCP_IP_Connection::readDataToServer()
{
//    qDebug() << "readDataToServer";
    // Эта ветка, когда есть передача.
    if (timerUploads->isActive())
    {
        tempMessage.append(sock->readAll()).toUInt();
    }
    // Передачи нет. Синхронизируем такты с новым таймером.
    else
    {
        if (oneTry)
        {
            tempMessage.clear();
            speed = 0;
            timerNoUploads->start(1000);
            oneTry = false;
        }
        tempMessage.append(sock->readAll()).toUInt();
        tempUploads = 0;
    }
}


// ============================================================================
// Сравнивает отправленные байты и принятые.
// И ведется статистика по другим параметрам.
void TCP_IP_Connection::analyzingData()
{
//    qDebug() << "analyzingData";
    messageFromServer.clear();
    messageFromServer.append(tempMessage);
    tempMessage.clear();
    timerUploads->stop();
    timerNoUploads->stop();
    if (sock->state() == 3)
    {
        uploadBytes = tempUploads;
        downloadBytes = messageFromServer.length();
        allUpBytes += uploadBytes;
        allDownBytes += downloadBytes;
        speed = downloadBytes;


        // ====================================================================
        // Извлечение ошибок
        for (quint32 i=0; i<downloadBytes; i++)
        {
            if (symbol == 256)
            {
                symbol = 0;
            }

            quint16 symbolFromMessage = (unsigned char)messageFromServer.at(i);

            if (symbol != symbolFromMessage)
            {
                errorsOfData++;
                symbol = messageFromServer.at(i);
            }
            symbol++;
        }


        // ====================================================================
        // Контроль скорости

        if (downloadBytes == uploadBytes && uploadBytes != 0)
        {
            if (sendPacket < 4096)
            {
                sendPacket = sendPacket * 2;
            }
            else if (sendPacket >= 4096 && sendPacket < 10240)
            {
                sendPacket = sendPacket + 2048;
            }
            else if (sendPacket >= 10240)
            {
                sendPacket = sendPacket + 512;
            }
        }
        else if (downloadBytes < uploadBytes && sendPacket != ORIGINAL_BUFFER_PACK)
        {
            if (sendPacket < 4096)
            {
                sendPacket = sendPacket / 2;
            }
            else if (sendPacket >= 4096 && sendPacket < 10240)
            {
                sendPacket = sendPacket - 2048;
            }
            else if (sendPacket >= 10240)
            {
                sendPacket = sendPacket - 512;
            }
        }


        // ====================================================================
        // Сюда идем тогда, когда нет передачи, только прием.
        if (!oneTry)
        {
            oneTry = true;
        }

        emit analyzingIsComplete();
    }
}


// ============================================================================
// Когда страбатывает вышеописанная функция мгновенного отсоединения,
// то вызывается этот слот, все обнуляется и сокет закрывается.
void TCP_IP_Connection::disconnectFromServer()
{
    uploadBytes = 0;
    downloadBytes = 0;
    allUpBytes = 0;
    allDownBytes = 0;
    errorsOfData = 0;
    sendPacket = ORIGINAL_BUFFER_PACK;
    speed = 0;
    symbol = 0;
    messageFromServer.clear();

    sock->close();
    emit isDisconnected();
}


// ============================================================================
// Обработка кнопки 'пауза'.
void TCP_IP_Connection::pauseOrContinue()
{
    if (!paused)
    {
        disconnect(this, SIGNAL(analyzingIsComplete()), this, SLOT(writeDataToServer()));
        paused = true;
    }
    else
    {
        connect(this, SIGNAL(analyzingIsComplete()), this, SLOT(writeDataToServer()));
        paused = false;
        emit analyzingIsComplete();
    }
}

// ============================================================================
// Обработка кнопки Cброс. Обнуляем параметры.
void TCP_IP_Connection::spilling()
{
    if (sock->state() == 3)
    {
        uploadBytes = 0;
        downloadBytes = 0;
        allUpBytes = 0;
        allDownBytes = 0;
        speed = 0;
        errorsOfData = 0;
        disconnect(this, SIGNAL(analyzingIsComplete()), this, SLOT(writeDataToServer()));
        emit analyzingIsComplete();
        connect(this, SIGNAL(analyzingIsComplete()), this, SLOT(writeDataToServer()));
    }
}
























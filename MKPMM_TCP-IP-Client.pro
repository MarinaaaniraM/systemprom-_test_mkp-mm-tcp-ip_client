#-------------------------------------------------
#
# Project created by QtCreator 2014-02-25T14:23:35
#
#-------------------------------------------------

QT       += core gui

TARGET = MKPMM_TCP-IP-Client
TEMPLATE = app

QT += network\
    xml

CONFIG += thread

SOURCES += main.cpp\
        mainwindow.cpp \
    tcp_ip_connection.cpp \
    socket_thread.cpp \
    graphics_form.cpp

HEADERS  += mainwindow.h \
    tcp_ip_connection.h \
    socket_thread.h \
    graphics_form.h

FORMS    += mainwindow.ui

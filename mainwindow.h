#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGridLayout>
#include <QLineEdit>
#include <QDomDocument>
#include <QFile>
#include <QTextCodec>
#include <socket_thread.h>
#include <graphics_form.h>


#define NUMBER_OF_CONNECTIONS 16

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void closeEvent(QCloseEvent *event);

private:
    Ui::MainWindow *ui;

    // Здесь будут столбцы с данными для каждого потока
    QWidget* centralWidget;
     // Сетка для компоновки столбцов с данными
    QGridLayout* grid;
    // Потоки данным берутся отсюда
    Socket_Thread* thread[NUMBER_OF_CONNECTIONS];
    // Форма для потоков берется отсюда
    Graphics_form* form[NUMBER_OF_CONNECTIONS];
    // Та же форма, только отсюда берем заголовки, когда это необходимо
    // (т.е. на каждый из двух рядов требуется один)
    Graphics_form* title[2];
    // Кнопка Сброс. Одна на все потоки
    QPushButton* spillingButton;
    // Кнопка Подключить все. Одна на все потоки
    QPushButton* connectAllButton;
    // Кнопка Отсоединить все. Одна на все потоки
    QPushButton* disconnectAllButton;

};

#endif // MAINWINDOW_H

#ifndef SOCKET_THREAD_H
#define SOCKET_THREAD_H

#include <QThread>
#include <QDebug>
#include <QTextCodec>
#include <tcp_ip_connection.h>
#include <graphics_form.h>

class Socket_Thread : public QThread
{
    Q_OBJECT
public:
    explicit Socket_Thread(QObject *parent = 0);
    void run();

    // Установка графической формы для работы с данными извне
    void setGraphicsForm(Graphics_form* f)
    {
        form = f;
    }

signals:
    void startThread();
    void spilling_signal();

private slots:
    void infoToForm();
    void serverDisconnected();
    void connectOrDisconnect();
    void spilling();
    void connectExternal();
    void disconnectExternal();

private:
    TCP_IP_Connection* connection;
    Graphics_form* form;
    bool stopped;
    bool allStopped;
};

#endif // SOCKET_THREAD_H

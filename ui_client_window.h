/********************************************************************************
** Form generated from reading UI file 'client_window.ui'
**
** Created: Wed Feb 26 00:15:21 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLIENT_WINDOW_H
#define UI_CLIENT_WINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Client_Window
{
public:
    QWidget *centralWidget;
    QLabel *statusLine1;
    QLabel *statusLine0;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *Client_Window)
    {
        if (Client_Window->objectName().isEmpty())
            Client_Window->setObjectName(QString::fromUtf8("Client_Window"));
        Client_Window->resize(931, 429);
        centralWidget = new QWidget(Client_Window);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        statusLine1 = new QLabel(centralWidget);
        statusLine1->setObjectName(QString::fromUtf8("statusLine1"));
        statusLine1->setGeometry(QRect(469, 9, 16, 16));
        statusLine0 = new QLabel(centralWidget);
        statusLine0->setObjectName(QString::fromUtf8("statusLine0"));
        statusLine0->setGeometry(QRect(9, 9, 16, 16));
        Client_Window->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(Client_Window);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        Client_Window->setStatusBar(statusBar);

        retranslateUi(Client_Window);

        QMetaObject::connectSlotsByName(Client_Window);
    } // setupUi

    void retranslateUi(QMainWindow *Client_Window)
    {
        Client_Window->setWindowTitle(QApplication::translate("Client_Window", "Client_Window", 0, QApplication::UnicodeUTF8));
        statusLine1->setText(QString());
        statusLine0->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Client_Window: public Ui_Client_Window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLIENT_WINDOW_H

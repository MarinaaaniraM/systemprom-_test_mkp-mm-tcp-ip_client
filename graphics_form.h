#ifndef GRAPHICS_FORM_H
#define GRAPHICS_FORM_H

#include <QWidget>
#include <QLineEdit>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QGroupBox>
#include <QTextCodec>
#include <QVBoxLayout>

class Graphics_form : public QWidget
{
    Q_OBJECT
public:
    explicit Graphics_form(QWidget *parent = 0);

    QVBoxLayout* getDataLayout();
    QVBoxLayout* getTitleLayout();


    // ========================================================================
    // Установка данных извне.
    void setPortName(QString name)      // Имя порта (порт устройства)
    {
        portName->setText("<b>" + name + "</b>");
    }
    void setAdress(QString adr)         // Ip адрес
    {
        address->setText(adr);
    }
    void setPort(quint16 p)             // Порт (порт адреса)
    {
        port->setText(QString::number(p));
    }


    // ========================================================================
    // Доступ к данным извне.
    QLineEdit* getAddress() const       // Форма ip адреса
    {
        return address;
    }
    QLineEdit* getPort() const          // Форма порта (порт адреса)
    {
        return port;
    }
    QLineEdit* getUploadBytes() const   // Форма переданных за сек байт
    {
        return uploadBytes;
    }
    QLineEdit* getDownloadBytes() const // Форма принятых за сек байт
    {
        return downloadbytes;
    }
    QLineEdit* getUpAllBytes() const    // Форма всех переданных байт
    {
        return upAllBytes;
    }
    QLineEdit* getDownAllBytes() const  // Форма всех принятый байт
    {
        return downAllBytes;
    }
    QLineEdit* getSpeedBytes() const    // Форма байты в секунду
    {
        return speedBytes;
    }
    QLineEdit* getSpeed() const         // Форма биты в секунду
    {
        return speed;
    }
    QLineEdit* getErrors() const        // Форма ошибки в процессе приема/передачи
    {
        return errors;
    }

    QPushButton* getConnectionButton() const// Кнопка Подключение
    {
        return connectionButton;
    }
    QPushButton* getPauseButton() const // Кнопка Пауза
    {
        return pauseButton;
    }
    QLabel* getStatusLine() const       // Строка состояния
    {
        return statusLine;
    }

private:
    QLabel *portName;
    QLineEdit *address;
    QLineEdit *port;
    QLineEdit *uploadBytes;
    QLineEdit *downloadbytes;
    QLineEdit *upAllBytes;
    QLineEdit *downAllBytes;
    QLineEdit *speedBytes;
    QLineEdit *speed;
    QLineEdit *errors;
    QPushButton *connectionButton;
    QPushButton *pauseButton;
    QLabel *statusLine;

    QLabel *labelNamePort;
    QLabel *labelAddress;
    QLabel *labelPort;
    QLabel *labelUpBytes;
    QLabel *labelDownBytes;
    QLabel *labelAll;
    QLabel *labelAllUpBytes;
    QLabel *labelAllDownBytes;
    QLabel *labelSpeedBytes;
    QLabel *labelSpeed;
    QLabel *labelErrors;
    QLabel *labelPause;
    QLabel *labelConnection;
    QLabel *labelStatus;

    QLabel *freeSpace;
    QVBoxLayout *dataLayout;
    QVBoxLayout *titleLayout;
};

#endif // GRAPHICS_FORM_H

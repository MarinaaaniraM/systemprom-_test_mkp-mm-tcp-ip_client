#ifndef TCP_IP_CONNECTION_H
#define TCP_IP_CONNECTION_H

#include <QObject>
#include <QTcpSocket>
#include <QTimer>
#include <QDebug>

#define ORIGINAL_BUFFER_PACK 64

class TCP_IP_Connection : public QObject
{
    Q_OBJECT
public:
    explicit TCP_IP_Connection(QObject *parent = 0);

    bool connectToHost(QString adress, quint16 port);
    void disconnectFromHost();

    // ========================================================================
    // Доступ к данным извне.
    QTcpSocket* getSocket() const       // Сокет
    {
        return sock;
    }
    quint32 getUploadBytes () const     // Переданные за сек байты
    {
        return uploadBytes;
    }
    quint32 getDownloadBytes () const   // Принятые за сек байты
    {
        return downloadBytes;
    }
    quint64 getAllUpBytes () const      // Все переданные байты
    {
        return allUpBytes;
    }
    quint64 getAllDownBytes () const    // Все принятые байты
    {
        return allDownBytes;
    }
    quint32 getErrorsOfData () const    // Ошибки в процессе приема/передачи
    {
        return errorsOfData;
    }
    quint64 getSpeed () const           // Байты в секунду
    {
        return speed;
    }
    
signals:
    void analyzingIsComplete();
    void isDisconnected();
    
public slots:
    void writeDataToServer();
    void readDataToServer();
    void analyzingData();
    void disconnectFromServer();
    void pauseOrContinue();
    void spilling();

private:
    QTcpSocket *sock;
    QTimer *timerUploads;
    QTimer *timerNoUploads;

    QByteArray messageFromServer;
    QByteArray tempMessage;
    quint32 uploadBytes;
    quint32 downloadBytes;
    quint64 allUpBytes;
    quint64 allDownBytes;
    quint32 errorsOfData;
    quint16 speed;
    quint16 symbol;
    quint16 nextSymbolToWrite;
    quint32 tempUploads;
    quint16 tempSpeed;
    quint32 sendPacket;
    bool paused;
    bool oneTry;


};

#endif // TCP_IP_CONNECTION_H

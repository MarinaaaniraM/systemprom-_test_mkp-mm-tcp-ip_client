#include "graphics_form.h"

Graphics_form::Graphics_form(QWidget *parent) :
    QWidget(parent)
{
    QTextCodec* codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForTr(codec);

    // ========================================================================
    // Поля с данными (В основном QLineEdit).
    portName = new QLabel(this);
    address = new QLineEdit(this);
    port = new QLineEdit(this);
    uploadBytes = new QLineEdit(this);
    downloadbytes = new QLineEdit(this);
    upAllBytes = new QLineEdit(this);
    downAllBytes = new QLineEdit(this);
    speedBytes = new QLineEdit(this);
    speed = new QLineEdit(this);
    errors = new QLineEdit(this);

    // ========================================================================
    // Прочие виджеты.
    connectionButton = new QPushButton(this);
    pauseButton = new QPushButton(tr("Пауза"), this);
    pauseButton->setCheckable(true);
    statusLine = new QLabel(this);


    // ========================================================================
    //Пустое поля для удобной визуализации.
    freeSpace = new QLabel(tr(" "), this);
}


// ============================================================================
// Эта функция выдает форму, в которой будут помещаться данные.
QVBoxLayout* Graphics_form::getDataLayout()
{
    // ========================================================================
    // Свойства полей с данными.
    address->setMinimumHeight(16);
    port->setMinimumHeight(16);
    uploadBytes->setReadOnly(true);
    uploadBytes->setFrame(false);
    uploadBytes->setMinimumHeight(17);
    downloadbytes->setReadOnly(true);
    downloadbytes->setFrame(false);
    downloadbytes->setMinimumHeight(17);
    upAllBytes->setReadOnly(true);
    upAllBytes->setFrame(false);
    upAllBytes->setMinimumHeight(17);
    downAllBytes->setReadOnly(true);
    downAllBytes->setFrame(false);
    downAllBytes->setMinimumHeight(17);
    speedBytes->setReadOnly(true);
    speedBytes->setFrame(false);
    speedBytes->setMinimumHeight(17);
    speed->setReadOnly(true);
    speed->setFrame(false);
    speed->setMinimumHeight(17);
    errors->setReadOnly(true);
    errors->setFrame(false);
    errors->setMinimumHeight(17);
    pauseButton->setMinimumHeight(14);
    connectionButton->setMinimumHeight(15);
    statusLine->setMinimumHeight(14);


    // ========================================================================
    // Вертикальный layout для компоновки полей с информацией и кнопками.
    dataLayout = new QVBoxLayout();

    dataLayout->addWidget(portName);
    dataLayout->addWidget(address);
    dataLayout->addWidget(port);
    dataLayout->addWidget(uploadBytes);
    dataLayout->addWidget(downloadbytes);
    dataLayout->addWidget(freeSpace);
    dataLayout->addWidget(upAllBytes);
    dataLayout->addWidget(downAllBytes);
    dataLayout->addWidget(speedBytes);
    dataLayout->addWidget(speed);
    dataLayout->addWidget(errors);
    dataLayout->addWidget(pauseButton);
    dataLayout->addWidget(connectionButton);
    dataLayout->addWidget(statusLine);
    dataLayout->addWidget(freeSpace);

    return dataLayout;
}



// ============================================================================
// Дополнительная функция, которая выведет заголовки (подписи) к полям.
// Опционально.
QVBoxLayout* Graphics_form::getTitleLayout()
{
    // ========================================================================
    // В данный момент они лишние и будут мешаться.
    portName->setHidden(true);
    address->setHidden(true);
    port->setHidden(true);
    uploadBytes->setHidden(true);
    downloadbytes->setHidden(true);
    upAllBytes->setHidden(true);
    downAllBytes->setHidden(true);
    speedBytes->setHidden(true);
    speed->setHidden(true);
    errors->setHidden(true);

    connectionButton->setHidden(true);
    pauseButton->setHidden(true);
    statusLine->setHidden(true);


    // ========================================================================
    // Заголовки для информационных полей.
    labelAddress = new QLabel(tr("Адрес"), this);
    labelPort = new QLabel(tr("Порт"), this);
    labelUpBytes = new QLabel(tr("Прд., б/с"), this);
    labelDownBytes = new QLabel(tr("Прм., б/с"), this);
    labelAll = new QLabel(tr("Всего:"), this);
    labelAllUpBytes = new QLabel(tr("Прд., б"), this);
    labelAllDownBytes = new QLabel(tr("Прм., б"), this);
    labelSpeedBytes = new QLabel(tr("Ск., б/с"), this);
    labelSpeed = new QLabel(tr("Ск., бит/с"), this);
    labelErrors = new QLabel(tr("Ошибки"), this);
    labelPause = new QLabel(tr("Передача"), this);
    labelConnection = new QLabel(tr("Соединение"), this);
    labelStatus = new QLabel(tr("Статус"), this);


    // ========================================================================
    // Вертикальный layout для помпоновки заголовков.
    titleLayout = new QVBoxLayout();

    titleLayout->addWidget(freeSpace);
    titleLayout->addWidget(labelAddress);
    titleLayout->addWidget(labelPort);
    titleLayout->addWidget(labelUpBytes);
    titleLayout->addWidget(labelDownBytes);
    titleLayout->addWidget(labelAll);
    titleLayout->addWidget(labelAllUpBytes);
    titleLayout->addWidget(labelAllDownBytes);
    titleLayout->addWidget(labelSpeedBytes);
    titleLayout->addWidget(labelSpeed);
    titleLayout->addWidget(labelErrors);
    titleLayout->addWidget(labelPause);
    titleLayout->addWidget(labelConnection);
    titleLayout->addWidget(labelStatus);
    titleLayout->addWidget(freeSpace);

    return titleLayout;
}

















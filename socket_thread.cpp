#include "socket_thread.h"

Socket_Thread::Socket_Thread(QObject *parent) :
    QThread(parent)
{
    QTextCodec* codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForTr(codec);

    stopped = false;
    allStopped = false;
    connection = new TCP_IP_Connection(this);
}


// ============================================================================
// С этой функции начинает свой путь поток.
void Socket_Thread::run()
{
    connect(form->getConnectionButton(), SIGNAL(clicked()), this, SLOT(connectOrDisconnect()));
    connect(form->getPauseButton(), SIGNAL(clicked()), connection, SLOT(pauseOrContinue()));
    connect(connection, SIGNAL(analyzingIsComplete()), this, SLOT(infoToForm()));
    connect(connection, SIGNAL(isDisconnected()), this, SLOT(serverDisconnected()));
    connect(this, SIGNAL(startThread()), this, SLOT(connectOrDisconnect()));
    connect(this, SIGNAL(spilling_signal()), connection, SLOT(spilling()));

    emit startThread();
    exec();
}


// ============================================================================
// Функция, которая передает данные сокета в форму.
void Socket_Thread::infoToForm()
{
    form->getUploadBytes()->setText(QString::number(connection->getUploadBytes()));
    form->getDownloadBytes()->setText(QString::number(connection->getDownloadBytes()));
    form->getUpAllBytes()->setText(QString::number(connection->getAllUpBytes()));
    form->getDownAllBytes()->setText(QString::number(connection->getAllDownBytes()));
    form->getSpeedBytes()->setText(QString::number(connection->getSpeed()));
    form->getSpeed()->setText(QString::number(connection->getSpeed() * 10));
    form->getErrors()->setText(QString::number(connection->getErrorsOfData()));
}


// ============================================================================
// Функция работающая по нажатию кнопки, которая устанавливает соединение.
void Socket_Thread::connectOrDisconnect()
{
    if (!stopped)
    {
        if (connection->connectToHost(form->getAddress()->text(), form->getPort()->text().toUInt()))
        {
            form->getStatusLine()->setText(tr("<i>Соединенно</i>"));
            form->getConnectionButton()->setText(tr("Отсоединить"));
            form->getPauseButton()->setEnabled(true);
            stopped = true;
        }
        else
        {
            form->getStatusLine()->setText(tr("<i>Нет соединения</i>"));
            form->getConnectionButton()->setText(tr("Подключить"));
            form->getPauseButton()->setEnabled(false);
            stopped = false;
        }
    }
    else
    {
        connection->disconnectFromHost();
        form->getConnectionButton()->setText(tr("Подключить"));
        form->getStatusLine()->setText(tr("<i>Нет соединения</i>"));
        form->getPauseButton()->setEnabled(false);
        form->getUploadBytes()->clear();
        form->getDownloadBytes()->clear();
        form->getUpAllBytes()->clear();
        form->getDownAllBytes()->clear();
        form->getSpeedBytes()->clear();
        form->getSpeed()->clear();
        form->getErrors()->clear();
        stopped = false;
    }
}


// ============================================================================
// Обработка кнопки Сброс.
void Socket_Thread::spilling()
{
    emit spilling_signal();
}


// ============================================================================
// Обновление данных формы при разрыве соединения.
void Socket_Thread::serverDisconnected()
{
    form->getUploadBytes()->clear();
    form->getDownloadBytes()->clear();
    form->getUpAllBytes()->clear();
    form->getDownAllBytes()->clear();
    form->getSpeedBytes()->clear();
    form->getSpeed()->clear();
    form->getErrors()->clear();

    form->getConnectionButton()->setText(tr("Подключить"));
    form->getStatusLine()->setText(tr("Нет соединения"));
}


// ============================================================================
// Обработка кнопки Подключить все.
void Socket_Thread::connectExternal()
{
    if (allStopped || !stopped)
    {
        stopped = false;
        allStopped = false;
        emit startThread();
    }
}


// ============================================================================
// Обработка кнопки отсоединить все.
void Socket_Thread::disconnectExternal()
{
    stopped = true;
    allStopped = true;
    emit startThread();
}


















